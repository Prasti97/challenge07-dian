import './landing.css';
import React from 'react';
import gambarLd from '../../images/img_car.png';
import gambarService from '../../images/img_service.png';
import iCheck from '../../images/Group 53.png';
import i24hr from '../../images/icon_24hrs.png';
import iPrice from '../../images/icon_price.png';
import iComplete from '../../images/icon_complete.png';
import iProf from '../../images/icon_professional.png';
import iRate from '../../images/Rate.png';
import iReview from '../../images/img_photo.png';
import iReview2 from '../../images/img_photo (2).png';
import iLb from '../../images/Left button.png';
import iRb from '../../images/Right button.png';
import Navbarku from '../navbar/navbar';
import Footerku from '../footer/footer';
import { Accordion } from 'react-bootstrap';
import { VictoryPie } from 'victory';

const Landing = () => {
  return (
    <div className="landing">
      <div className="main">
        <Navbarku />

        <div className="hero row">
          <div className="box2 col">
            <h1>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
            <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
            <button href="#">Mulai Sewa Mobil</button>
          </div>
          <div className="img-car col">
            <img src={gambarLd} alt="" />
          </div>
        </div>
      </div>

      <div className="our-servicesku">
        <div className="row our-services">
          <div className="col left-image img-fluid imgService">
            <img src={gambarService} alt="..." />
          </div>
          <div className="col kotak9">
            <h3>Best Car Rental for any kind of trip in (Lokasimu)!</h3>
            <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
            <p>
              <span>
                <img src={iCheck} alt="..." />
              </span>
              Sewa Mobil Dengan Supir di Bali 12 Jam
            </p>
            <p>
              <span>
                <img src={iCheck} alt="..." />
              </span>
              Sewa Mobil Lepas Kunci di Bali 24 Jam
            </p>
            <p>
              <span>
                <img src={iCheck} alt="..." />
              </span>
              Sewa Mobil Jangka Panjang Bulanan
            </p>
            <p>
              <span>
                <img src={iCheck} alt="..." />
              </span>
              Gratis Antar - Jemput Mobil di Bandara
            </p>
            <p>
              <span>
                <img src={iCheck} alt="..." />
              </span>
              Layanan Airport Transfer / Drop In Out
            </p>
          </div>
        </div>
      </div>

      <div className="chart">
        <h3>Presentase Tipe Mobil Paling Banyak Diminati</h3>
        <VictoryPie
          style={{
            data: {
              fillOpacity: 0.9,
            },
            labels: {
              fontSize: 5,
              fill: '#000',
            },
          }}
          padding={{ top: 10, bottom: 30 }}
          height={150}
          colorScale={['coral', 'cadetblue', 'plum']}
          data={[
            { x: 'Sedan', y: 35 },
            { x: 'MPV', y: 28 },
            { x: 'Innova', y: 32 },
          ]}
        />
      </div>

      <div className="why-usku">
        <div className="why-us">
          <div className=" why-us-title">
            <h3>Why Us?</h3>
            <p>Mengapa harus pilih Binar Car Rental?</p>
          </div>
          <div className="row why-us-card">
            <div className="col">
              <div className="card">
                <img src={iComplete} width="32" alt="..." />
                <div className="card-body">
                  <h4>Mobil Lengkap</h4>
                  <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                </div>
              </div>
            </div>
            <div className="col">
              <div className="card">
                <img src={iPrice} width="32" alt="..." />
                <div className="card-body">
                  <h4>Harga Murah</h4>
                  <p>Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                </div>
              </div>
            </div>
            <div className="col">
              <div className="card">
                <img src={i24hr} width="32" alt="..." />
                <div className="card-body">
                  <h4>Layanan 24 Jam</h4>
                  <p>Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                </div>
              </div>
            </div>
            <div className="col">
              <div className="card">
                <img src={iProf} width="32" alt="..." />
                <div className="card-body">
                  <h4>Sopir Profesional</h4>
                  <p>Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="testimonial">
        <div className="testimonial-title ">
          <h3>Testimonial</h3>
          <p>Berbagai review positif dari para pelanggan kami</p>
        </div>
        <div className="swiper">
          <div className="swiper-wrapper ">
            <div className="swiper-slide ">
              <div className="testimonial-card">
                <div className="testimonial-card-photo">
                  <img src={iReview} alt="..." />
                </div>
                <div className="testimonial-info">
                  <img src={iRate} alt="..." />
                  <p>
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                  </p>
                  <p>
                    <strong> Dee 32, Bromo</strong>
                  </p>
                </div>
              </div>
            </div>
            <div className="swiper-slide ">
              <div className="testimonial-card">
                <div className="testimonial-card-photo">
                  <img src={iReview2} alt="..." />
                </div>
                <div className="testimonial-info">
                  <img src={iRate} alt="..." />
                  <p>
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                  </p>
                  <p>
                    <strong>John Dee 32, Bromo</strong>
                  </p>
                </div>
              </div>
            </div>
            <div className="swiper-slide ">
              <div className="testimonial-card">
                <div className="testimonial-card-photo">
                  <img src={iReview} alt="..." />
                </div>
                <div className="testimonial-info">
                  <img src={iRate} alt="..." />
                  <p>
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                  </p>
                  <p>
                    <strong>John Dee 32, Bromo</strong>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="testimonial-btn d-flex justify-content-center">
          <div>
            <img src={iLb} alt="..." className="swiper-button-prev" />
          </div>
          <div>
            <img src={iRb} alt="..." className="swiper-button-next" />
          </div>
        </div>
      </div>

      <div className="bannerku">
        <div className="banner">
          <h3>Sewa Mobil di (Lokasimu) Sekarang</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
          <button href="#">Mulai Sewa Mobil</button>
        </div>
      </div>

      <div className="faqku">
        <div className="row faq">
          <div className="col">
            <h3>Frequently Asked Question</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
          </div>
          <div className="col">
            <Accordion className="textku">
              <Accordion.Item eventKey="0">
                <Accordion.Header>Apa saja syarat yang dibutuhkan?</Accordion.Header>
                <Accordion.Body>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                  laborum.
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="1">
                <Accordion.Header>Berapa hari minimal sewa mobil lepas kunci?</Accordion.Header>
                <Accordion.Body>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                  laborum.
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="2">
                <Accordion.Header>Berapa hari minimal sewa mobil lepas kunci?</Accordion.Header>
                <Accordion.Body>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                  laborum.
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="3">
                <Accordion.Header>Berapa hari sebelumnya sabaiknya booking sewa mobil?</Accordion.Header>
                <Accordion.Body>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                  laborum.
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="4">
                <Accordion.Header>Apakah Ada biaya antar-jemput?</Accordion.Header>
                <Accordion.Body>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                  laborum.
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="5">
                <Accordion.Header>Bagaimana jika terjadi kecelakaan?</Accordion.Header>
                <Accordion.Body>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                  laborum.
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>
          </div>
        </div>
      </div>
      <Footerku />
    </div>
  );
};

export default Landing;
