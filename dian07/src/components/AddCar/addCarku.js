import './addCarku.css';
import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const AddCarku = () => {
  const [inputNewCar, setInputNewCar] = useState({
    name: '',
    price: '',
    image: '',
  });
  const handleInput = (e) => {
    setInputNewCar({
      ...inputNewCar,
      [e.target.id]: e.target.value,
    });
    console.log(e.target.value);
  };

  // const handleUpload = (e) => {
  //   console.log(e);
  // };

  const navigate = useNavigate();
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post('https://rent-cars-api.herokuapp.com/admin/car', inputNewCar);
      const newCar = {
        name: res.data.name,
        price: res.data.price,
        image: res.data.image,
      };
      localStorage.setItem('userInfo', JSON.stringify(newCar));
      navigate('/listcar');
    } catch (error) {
      console.error(error);
      alert('AddCar Failed.Please Try Again!');
    }
  };

  return (
    <div className="addCarku">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">ListCar</a>
          </li>
          <li class="breadcrumb-item">
            <a href="#">ListCar</a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">
            Add New Car
          </li>
        </ol>
      </nav>
      <p>Add New Car</p>
      <div className="kotak4">
        <div className="kotak5">
          <Form>
            <Form.Group className="mb-3 d-flex row" controlId="formBasicEmail">
              <Form.Label className="col label">
                Nama<span>*</span>
              </Form.Label>
              <Form.Control className="col" type="text" placeholder="Placeholder" id="name" value={inputNewCar.name} onChange={handleInput} />
            </Form.Group>
            <Form.Group className="mb-3 d-flex row" controlId="formBasicPassword">
              <Form.Label className="col label">
                Harga<span>*</span>
              </Form.Label>
              <Form.Control className="col" type="number" placeholder="Placeholder" id="price" value={inputNewCar.price} onChange={handleInput} />
            </Form.Group>
            <Form.Group className="mb-3 d-flex row" controlId="formBasicPassword">
              <Form.Label className="col label">
                Foto<span>*</span>
              </Form.Label>
              <Form.Control className="col" type="file" placeholder="Placeholder" id="image" value={inputNewCar.image} onChange={handleInput} />
              {/* <input type="file" id="image" accept="image/png, image/jpeg" value={inputNewCar.image} onChange={handleInput}></input> */}
              <span id="tKecil">File size max 2MB</span>
            </Form.Group>
            <div className="row">
              <Form.Label className="col label">Start Rent</Form.Label>
              <Form.Label className="col label">-</Form.Label>
            </div>
            <div className="row">
              <Form.Label className="col label">Finish Rent</Form.Label>
              <Form.Label className="col label">-</Form.Label>
            </div>
            <div className="row">
              <Form.Label className="col label">Created at</Form.Label>
              <Form.Label className="col label">-</Form.Label>
            </div>
            <div className="row">
              <Form.Label className="col label">Updated at</Form.Label>
              <Form.Label className="col label">-</Form.Label>
            </div>
          </Form>
        </div>
      </div>

      <div className="btnku ">
        <a href="/listcar">
          <Button variant="primary" type="submit" className="bCancel">
            Cancel
          </Button>
        </a>
        <a href="#">
          <Button variant="primary" type="submit" className="bSave" onClick={handleSubmit}>
            Save
          </Button>
        </a>
      </div>
    </div>
  );
};

export default AddCarku;
