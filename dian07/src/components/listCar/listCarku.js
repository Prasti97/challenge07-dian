import './listCarku.css';
import React, { useEffect, useState } from 'react';
import { Card, Button } from 'react-bootstrap';
// import Axios from 'axios';
import ikey from '../../images/fi_key.png';
import iclock from '../../images/fi_clock.png';
import idelt from '../../images/fi_trash-2.png';
import iedit from '../../images/fi_edit.png';
import iAdd from '../../images/fi_plus.png';
import { useDispatch, useSelector } from 'react-redux';
import { getCars } from '../../store/cars/carsSlice';

const ListCarku = () => {

  const dispatch = useDispatch();
  const { cars, isLoading } = useSelector((state) => state.cars);

  useEffect(() => {
    dispatch(getCars());
  }, [dispatch]);
  console.log(cars);

  const listcar = cars.map((car) => {
    return car;
  });

  console.log(listcar);

  return (
    <div className="listCarku">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">ListCar</a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">
            ListCar
          </li>
        </ol>
      </nav>

      <div className="d-flex justify-content-between">
        <p>List Car</p>

        <button className="btnAdd">
          <span>
            <img src={iAdd} alt="..." />
          </span>
          <a href="/addCar">Add New Car</a>
        </button>
      </div>

      <div id="btn-group">
        <button className="btn activeBtn">All</button>
        <button className="btn">Small</button>
        <button className="btn">Medium</button>
        <button className="btn">Large</button>
      </div>

      <div className="cards row justify-content-around">
        {isLoading && (
          <div className="spiner text-center">
            <div className="spinner-border" role="status">
              <span class="visually-hidden">Loading...</span>
            </div>
            <div className="spinner-grow" role="status">
              <span class="visually-hidden">Loading...</span>
            </div>
          </div>
        )}
        {listcar.length > 0 &&
          listcar.map((item) => {
            return (
              <Card className="card col-auto">
                <a href={`/detailCar/${item.id}`}>
                  <Card.Img variant="top" src={item.image} className="imgCar" />
                  <Card.Body>
                    <Card.Title className="title">{item.name}</Card.Title>
                    <Card.Text className="price">Rp. {item.price} / hari</Card.Text>
                    <div className="icons">
                      <p>
                        <span>
                          <img src={ikey} className="icons" alt="..." />
                        </span>
                        Start rent - Finish rent
                      </p>
                      <p>
                        <span>
                          <img src={iclock} className="icons" alt="..." />
                        </span>
                        Updated at 4 April 2022, 09.00
                      </p>
                    </div>
                    <div className="btns d-flex justify-content-between">
                      <Button variant="primary" className="delete">
                        <span>
                          <img src={idelt} className="icons" alt="..." />
                        </span>
                        Delete
                      </Button>
                      <Button variant="primary" className="edit">
                        <span>
                          <img src={iedit} className="icons" alt="..." />
                        </span>
                        Edit
                      </Button>
                    </div>
                  </Card.Body>
                </a>
              </Card>
            );
          })}
      </div>
    </div>
  );
};

export default ListCarku;
