import './isiSignUp.css';
import React, { useState } from 'react';
import gambarSI from '../../images/image 2.png';
import gambarLogo from '../../images/Rectangle 62.png';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const IsiSignUp = () => {
  const [inputSignUp, setInputSignUp] = useState({
    first_name: '',
    last_name: '',
    email: '',
    password: '',
    password_confirmation: '',
  });
  const handleInputSU = (e) => {
    setInputSignUp({
      ...inputSignUp,
      [e.target.id]: e.target.value,
    });
    console.log(e.target.value);
  };
  const navigate = useNavigate();
  const handleSubmitSU = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post('http://notflixtv.herokuapp.com/api/v1/users', inputSignUp);
      console.log(res);
      localStorage.setItem('token', res.data.data.token);
      if (res.data.status === true) {
        navigate('/signIn');
      }
    } catch (error) {
      console.error(error);
      alert('SignUp Failed.Please Try Again!');
    }
  };

  return (
    <div className="isiSignUp">
      <div className="row">
        <div className="bg-mobil col-8">
          <img src={gambarSI} alt="" />
        </div>
        <div className="box1 col-4 d-flex justify-content-center align-items-center">
          <div className="mb-3 box2">
            <div className="kotak1">
              <img src={gambarLogo} alt="" />
            </div>

            <form>
              <h1>Create New Account</h1>

              <div id="invalidBox">
                <p>Masukkan username dan password yang benar. Perhatikan penggunaan huruf kapital.</p>
              </div>

              <label for="first_name" className="form-label">
                FirstName
              </label>
              <input type="text" className="form-control" id="first_name" value={inputSignUp.first_name} onChange={handleInputSU} placeholder="Input your firstname" required />

              <label for="last_name" className="form-label">
                LastName
              </label>
              <input type="text" className="form-control" id="last_name" value={inputSignUp.last_name} onChange={handleInputSU} placeholder="Input your lastname" required />

              <label for="email" className="form-label">
                Email address
              </label>
              <input type="email" className="form-control" id="email" value={inputSignUp.email} onChange={handleInputSU} placeholder="Input your email" required />

              <label for="password" className="form-label">
                Password
              </label>
              <input type="password" className="form-control" id="password" value={inputSignUp.password} onChange={handleInputSU} placeholder="Input your password" required />

              <label for="password_confirmation" className="form-label">
                Password confirmation
              </label>
              <input type="password" className="form-control" id="password_confirmation" value={inputSignUp.password_confirmation} onChange={handleInputSU} placeholder="Input your password confirmation" required />

              <button type="submit" onClick={handleSubmitSU}>
                Sign Up
              </button>
            </form>

            <div className="text1">
              <p>
                Don't have an account?
                <span>
                  <a href="/signIn"> Login</a>
                </span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default IsiSignUp;
