import './detailCarku.css';
import React, { useState, useEffect } from 'react';
// import { useParams } from 'react-router-dom';
// import Axios from 'axios';
import iUser from '../../images/fi_users.png';
import iStg from '../../images/fi_settings.png';
import iCalender from '../../images/fi_calendar.png';
import ModalImage from 'react-modal-image';
import { useDispatch, useSelector } from 'react-redux';
import { getDetail } from '../../store/cars/detailSlice';
import { useParams } from 'react-router-dom';

const DetailCarku = () => {
  const { id } = useParams();
  console.log(id);

  // const url = `https://rent-cars-api.herokuapp.com/admin/car/${id}`;
  // const [details, setDetails] = useState({
  //   cars: [],
  // });

  // useEffect(() => {
  //   Axios.get(url).then((res) => {
  //     console.log(res.data);
  //     setDetails({ cars: res.data });
  //     console.log();
  //   });
  // });

  // const item = details.cars;

  const dispatch = useDispatch();
  const { detail, isLoading } = useSelector((state) => state.detail);

  useEffect(() => {
    dispatch(getDetail(id));
  }, [dispatch]);

  console.log(detail);
  const item = detail;
  return (
    <div className="detailCarku">
      <div className="isiDetail row">
        <div className="box3 col-8">
          <div className="iBox3">
            <h5>Tentang Paket</h5>
            <h6>Include</h6>
            <ul>
              <li> Apa saja yang termasuk dalam paket misal durasi max 12 jam</li>
              <li>Sudah termasuk bensin selama 12 jam</li>
              <li>Sudah termasuk Tiket Wisata</li>
              <li>Sudah termasuk pajak</li>
            </ul>
            <h6>Exclude</h6>
            <ul>
              <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
              <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
              <li>Tidak termasuk akomodasi penginapan</li>
            </ul>
            <h5>Refund, Reschedule, Overtime</h5>
            <ul>
              <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
              <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
              <li>Tidak termasuk akomodasi penginapan</li>
            </ul>
          </div>

          <div className="btnku">
            <a href={`/${item.id}/invoice`}>
              <button>Lanjutkan Pembayaran</button>
            </a>
          </div>
        </div>
        <div className="box4 col-4">
          <div className="iBox4">
            <div className="gambar">
              <ModalImage small={item.image} large={item.image} />
            </div>
            <h5>{item.name}</h5>
            <div className="icons d-flex">
              <p>
                <span>
                  <img src={iUser} className="icons" alt="..." />
                </span>
                4 orang
              </p>
              <p>
                <span>
                  <img src={iStg} className="icons" alt="..." />
                </span>
                Manual
              </p>
              <p>
                <span>
                  <img src={iCalender} className="icons" alt="..." />
                </span>
                Tahun 2020
              </p>
            </div>
            <div className="total d-flex justify-content-between">
              <h6>Total</h6>
              <h5>Rp. {item.price}</h5>
            </div>
            <div className="btnku2">
              <a href={`/${item.id}/invoice`}>
                <button>Lanjutkan Pembayaran</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailCarku;
