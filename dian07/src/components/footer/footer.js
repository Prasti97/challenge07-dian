import './footer.css';
import iFb from '../../images/icon_facebook.png';
import iIns from '../../images/icon_instagram.png';
import iMail from '../../images/icon_mail.png';
import iTwc from '../../images/icon_twitch.png';
import iTwt from '../../images/icon_twitter.png';

const Footerku = () => {
  return (
    <div className="footerku">
      <div className="footer d-flex justify-content-between">
        <div className="address">
          <ul>
            <li>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</li>
            <li>binarcarrental@gmail.com</li>
            <li>081-233-334-808</li>
          </ul>
        </div>
        <div className="navigasi">
          <ul>
            <li>Our Services</li>
            <li>Why Us</li>
            <li>Testimonial</li>
            <li>FAQ</li>
          </ul>
        </div>
        <div className="sosmed">
          <p>Connect with us</p>
          <ul class="d-flex justify-content-between">
            <li>
              <img src={iFb} alt="" />
            </li>
            <li>
              <img src={iIns} alt="" />
            </li>
            <li>
              <img src={iTwt} alt="" />
            </li>
            <li>
              <img src={iMail} alt="" />
            </li>
            <li>
              <img src={iTwc} alt="" />
            </li>
          </ul>
        </div>
        <div className="copyright">
          <p>Copyright Binar 2022</p>
          <div className="box7"></div>
        </div>
      </div>
    </div>
  );
};

export default Footerku;
