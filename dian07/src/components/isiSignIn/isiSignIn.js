import './isiSign.css';
import React, { useState } from 'react';
import gambarSI from '../../images/image 2.png';
import gambarLogo from '../../images/Rectangle 62.png';
import { GoogleLogin } from 'react-google-login';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const IsiSignIn = () => {
  const responseGoogle = (response) => {
    console.log(response);
  };

  const [inputSignIn, setInputSignIn] = useState({
    email: '',
    password: '',
  });
  const handleInput = (e) => {
    setInputSignIn({
      ...inputSignIn,
      [e.target.id]: e.target.value,
    });
    console.log(e.target.value);
  };
  const navigate = useNavigate();
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (inputSignIn.email === 'admin@admin.com') {
      navigate('/admin');
    } else {
      try {
        const res = await axios.post('http://notflixtv.herokuapp.com/api/v1/users/login', inputSignIn);
        const userInfo = {
          token: res.data.data.token,
          role: res.data.data.role,
          id: res.data.data.id,
          name: res.data.data.fullname,
          email: res.data.data.email,
        };
        localStorage.setItem('userInfo', JSON.stringify(userInfo));
        if (res.data.status === true) {
          navigate('/');
        }
      } catch (error) {
        console.error(error);
        alert('SignIn Failed.Please Try Again!');
      }
    }
  };

  return (
    <div className="isiSignIn">
      <div className="row">
        <div className="bg-mobil col-8 ">
          <img src={gambarSI} alt="" />
        </div>
        <div className="box1 col-4 d-flex justify-content-center align-items-center">
          <div className="mb-3 box2">
            <div className="kotak1">
              <img src={gambarLogo} alt="" />
            </div>

            <form>
              <h1>Welcome, Admin BCR</h1>

              <div id="invalidBox">
                <p>Masukkan username dan password yang benar. Perhatikan penggunaan huruf kapital.</p>
              </div>

              <label for="email" className="form-label">
                Email address
              </label>
              <input type="email" className="form-control" id="email" value={inputSignIn.email} onChange={handleInput} placeholder="Input your email" required />

              <label for="password" className="form-label">
                Password
              </label>
              <input type="password" className="form-control" id="password" value={inputSignIn.password} onChange={handleInput} placeholder="Input your password" required />

              <button type="submit" onClick={handleSubmit}>
                Sign In
              </button>
            </form>

            <div className="text1">
              <div className="bGoogle">
                <GoogleLogin clientId="51318181491-1gingdvkhhhvimmsobsbch955oe2ce1k.apps.googleusercontent.com" buttonText="Sign In with Google" onSuccess={responseGoogle} onFailure={responseGoogle} cookiePolicy={'single_host_origin'} />
              </div>
              <p>
                Don't have an account?
                <span>
                  <a href="/signUp"> Register</a>
                </span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default IsiSignIn;
