import './invoiceku.css';
import React from 'react';
import { useParams } from 'react-router-dom';
import iCheck from '../../images/success.png';
import iUnduh from '../../images/fi_download.png';
import { Worker, Viewer } from '@react-pdf-viewer/core';
import '@react-pdf-viewer/core/lib/styles/index.css';
import filePDF from '../../assets/pdf-test.pdf';

const Invoiceku = () => {
  const { id } = useParams();
  console.log(id);
  return (
    <div className="invoiceku">
      <div className="invoice">
        <div className="kotak7">
          <img src={iCheck} alt="" className="igmbr" />
          <p>Pembayaran Berhasil!</p>
          <p className="tKecil">Tunjukan invoice ini ke petugas BRI di titik temu</p>
        </div>
        <div className="kotak8">
          <div className="kotak9 d-flex justify-content-between">
            <p>Invoice</p>
            <button className="btnUnduh">
              <span>
                <img src={iUnduh} alt="..." />
              </span>
              Unduh
            </button>
          </div>
          <p className="tKecil2">*no invoice</p>
          <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.13.216/build/pdf.worker.min.js">
            <div className="pdfView">
              <Viewer fileUrl={filePDF} />
            </div>
          </Worker>
        </div>
      </div>
    </div>
  );
};

export default Invoiceku;
