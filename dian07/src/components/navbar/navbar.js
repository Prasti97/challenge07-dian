import './navbar.css';
import React from 'react';
import { useParams } from 'react-router-dom';
import iBack from '../../images/fi_arrow-left.png';
import iGmbr from '../../images/Frame 19.png';

const Navbarku = (props) => {
  const page = props.page;
  const { id } = useParams();
  console.log(id);

  return (
    <div className="navbarku">
      <nav className="navbar navbar-expand-lg navbar-dark">
        <div className="container">
          <div className="navbar-brand" href="#"></div>

          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav ms-auto">
              <li className="nav-item">
                <a className="nav-link text-dark" href="#">
                  Our Service
                </a>
              </li>
              <li class="nav-item">
                <a className="nav-link text-dark" href="#">
                  Why Us
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-dark" href="#">
                  Testimonial
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-dark" href="#">
                  FAQ
                </a>
              </li>
              <li class="nav-item">
                <button>
                  <a href="/signUp">Register</a>
                </button>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      {page === 'invoice' && (
        <div className="invo row">
          <div className="col-md-auto">
            <a href={`/detailCar/${id}`}>
              <img src={iBack} alt="..." />
            </a>
          </div>
          <div className="col">
            <p>Tiket</p>
            <span>Order ID: xxxxxx{id}</span>
          </div>
          <div className="col-md-auto">
            <img src={iGmbr} alt="..." />
          </div>
        </div>
      )}
    </div>
  );
};

export default Navbarku;
