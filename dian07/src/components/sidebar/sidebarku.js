import './sidebarku.css';
import React, { useState } from 'react';
import { VscChromeClose, VscThreeBars } from 'react-icons/vsc';
import { BiHomeAlt } from 'react-icons/bi';
import { FiTruck } from 'react-icons/fi';
import gambarLogo from '../../images/Rectangle 62.png';
import gambarLogo2 from '../../images/Rectangle 63.png';
import imgUser from '../../images/Group 15.png';
import { Form, FormControl, Button, Dropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ListCarku from '../listCar/listCarku';
import AddCarku from '../AddCar/addCarku';
import Dashboardku from '../dashboard/dashboardku';

const Sidebarku = (props) => {
  const page = props.page;

  const [show, setShow] = useState(false);
  const handleClick = () => {
    setShow(!show);
  };
  const [showSide, setShowSide] = useState(false);
  const [showSide2, setShowSide2] = useState(false);
  const handleSide = () => {
    setShowSide(true);
    setShowSide2(false);
  };
  const handleSide2 = () => {
    setShowSide(false);
    setShowSide2(true);
  };
  return (
    <div className="sidebarku">
      <main className={show ? 'space-toogle' : null}>
        <header className={`header ${show ? 'space-toogle' : null}`}>
          <div className="kotak1 d-flex">
            {show ? (
              <div className="header-toogle" onClick={handleClick}>
                <VscChromeClose />
              </div>
            ) : (
              <div className="header-toogle" onClick={handleClick}>
                <VscThreeBars />
              </div>
            )}
            <div className="logo">
              <img src={gambarLogo} />
            </div>
          </div>
          <div className="kotak2 d-flex">
            <Form className="d-flex kotak3">
              <FormControl type="text" placeholder="Search" className="me-2" id="search" aria-label="Search" />
              <Button id="btn">Search</Button>
            </Form>
            <Dropdown>
              <Dropdown.Toggle variant="success" id="dropdown-basic">
                <img src={imgUser} alt="" id="imgUser" />
                <span>Unis Badri</span>
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </header>

        <aside className={`sideBar ${show ? 'showku' : null}`}>
          <nav className="navku">
            <div>
              <div className="logo2">
                <img src={gambarLogo2} className="logo-icon" />
              </div>

              <div className="nav-list">
                <div className={`lis ${showSide ? 'active' : null}`} onClick={handleSide}>
                  <BiHomeAlt className="lis-icon" />
                  <p className="lis-name">Dashboard</p>
                </div>
                <div className={`lis ${showSide2 ? 'active' : null}`} onClick={handleSide2}>
                  <FiTruck className="lis-icon" />
                  <p className="lis-name">List Car</p>
                </div>
              </div>
              {showSide ? (
                <ul className={`side ${!show ? 'sideClose' : null}`}>
                  <li>DASHBOARD</li>
                  <li>
                    <a href="/dashboard">Dashboard</a>
                  </li>
                </ul>
              ) : null}
              {showSide2 ? (
                <ul className={`side ${!show ? 'sideClose' : null}`}>
                  <li>CARS</li>
                  <li>
                    <a href="/listcar">List Car</a>
                  </li>
                </ul>
              ) : null}
            </div>
          </nav>
        </aside>

        {page === 'dashboard' && (
          <div className="dashB">
            <Dashboardku />
          </div>
        )}
        {page === 'listCar' && (
          <div className="listcar">
            <ListCarku />
          </div>
        )}
        {page === 'addCar' && (
          <div className="addcar">
            <AddCarku />
          </div>
        )}
      </main>
    </div>
  );
};

export default Sidebarku;
