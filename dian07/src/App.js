import React from 'react';
import logo from './logo.svg';
import './App.css';

import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './pages/home';
import SignIn from './pages/signIn';
import SignUp from './pages/signUp';
import Admin from './pages/admin';
import Dashboard from './pages/dashboard';
import ListCar from './pages/listCar';
import AddCar from './pages/addCar';
import DetailCar from './pages/detailCar';
import Invoice from './pages/invoice';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/signIn" element={<SignIn />} />
          <Route path="/signUp" element={<SignUp />} />
          <Route path="/admin" element={<Admin />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/listCar" element={<ListCar />} />
          <Route path="/addCar" element={<AddCar />} />
          <Route path="/detailCar/:id" element={<DetailCar />} />
          <Route path="/:id/invoice" element={<Invoice />} />
          {/* <Route path="/invoice" element={<Invoice />} /> */}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
