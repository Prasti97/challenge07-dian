import React from 'react';
import Sidebarku from '../components/sidebar/sidebarku';

const AddCar = () => {
  return (
    <div className="AddCar">
      <Sidebarku page="addCar" />
    </div>
  );
};

export default AddCar;
