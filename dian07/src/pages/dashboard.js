import React from 'react';
import Sidebarku from '../components/sidebar/sidebarku';

const Dashboard = () => {
  return (
    <div className="Dashboard">
      <Sidebarku page="dashboard" />
    </div>
  );
};

export default Dashboard;
