import React from 'react';
import IsiSignIn from '../components/isiSignIn/isiSignIn';

const SignIn = () => {
  return (
    <div>
      <IsiSignIn />
    </div>
  );
};

export default SignIn;
