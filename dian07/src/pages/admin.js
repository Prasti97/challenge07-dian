import React from 'react';
import Sidebarku from '../components/sidebar/sidebarku';

const Admin = () => {
  return (
    <div className="Admin">
      <Sidebarku />
    </div>
  );
};

export default Admin;
