import React from 'react';
import Navbarku from '../components/navbar/navbar';
import Footerku from '../components/footer/footer';
import Invoiceku from '../components/invoice/invoiceku';

const Invoice = () => {
  return (
    <div className="invoice">
      <Navbarku page="invoice" />
      <Invoiceku />
      <Footerku />
    </div>
  );
};

export default Invoice;
