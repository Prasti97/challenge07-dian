import React from 'react';
import Navbarku from '../components/navbar/navbar';
import Footerku from '../components/footer/footer';
import DetailCarku from '../components/detailCar/detailCarku';

const DetailCar = () => {
  return (
    <div className="detailCar">
      <Navbarku />
      <DetailCarku />
      <Footerku />
    </div>
  );
};

export default DetailCar;
